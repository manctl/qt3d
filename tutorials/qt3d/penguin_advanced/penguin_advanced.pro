TEMPLATE = app
TARGET = penguin_advanced
CONFIG += qt warn_on
QT += 3d
SOURCES = modelview.cpp \
    main.cpp
HEADERS = modelview.h
RESOURCES = model_advanced.qrc
