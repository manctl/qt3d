TEMPLATE = app
TARGET = penguin
CONFIG += qt warn_on
QT += 3d
SOURCES = modelview.cpp \
    main.cpp
HEADERS = modelview.h
RESOURCES = model.qrc
