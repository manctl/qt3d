TEMPLATE = subdirs
SUBDIRS += \
    basket \
    cube \
    forest \
    lander \
    matrix_animation \
    monkeygod \
    moon \
    photoroom \
    shaders \
    sphere \
    animations
