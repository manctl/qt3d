TARGET = tst_qspheremesh
CONFIG += testcase
TEMPLATE=app
QT += testlib 3d 3dquick
QT += qml quick
CONFIG += warn_on

SOURCES += tst_qspheremesh.cpp
