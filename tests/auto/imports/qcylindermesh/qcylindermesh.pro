TARGET = tst_qcylindermesh
CONFIG += testcase
TEMPLATE=app
QT += testlib 3d 3dquick
QT += qml quick
CONFIG += warn_on

SOURCES += tst_qcylindermesh.cpp
