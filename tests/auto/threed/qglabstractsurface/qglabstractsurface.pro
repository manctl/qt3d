TARGET = tst_qglabstractsurface
CONFIG += testcase
TEMPLATE=app
QT += testlib 3d
CONFIG += warn_on

SOURCES += tst_qglabstractsurface.cpp

CONFIG+=insignificant_test  # See QTBUG-25277
