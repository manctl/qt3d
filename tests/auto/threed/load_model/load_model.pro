TARGET = tst_load_model
CONFIG += testcase
TEMPLATE=app
QT += testlib 3d
CONFIG += warn_on

SOURCES += tst_load_model.cpp

RESOURCES += \
    load_model.qrc
