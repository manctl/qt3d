TARGET = tst_qglmaterial
CONFIG += testcase
TEMPLATE=app
QT += testlib 3d
CONFIG += warn_on
CONFIG += insignificant_test  # See QTBUG-25757

SOURCES += tst_qglmaterial.cpp
