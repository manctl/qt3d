TEMPLATE = subdirs
SUBDIRS +=  displaymodel \
            model3ds \
            navigation1 \
            rotation \
            scaling \
            submesh \
            translation \
            transformations \
            animations \
            qrc \
            basic \
            background \
            fullreferencing \
            shapes
